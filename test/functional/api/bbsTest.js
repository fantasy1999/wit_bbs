const chai = require('chai')
const expect = chai.expect
const request = require('supertest')
const MongoMemoryServer = require('mongodb-memory-server').MongoMemoryServer
const User = require('../../../models/User')
const Post = require('../../../models/Post')
const Comment = require('../../../models/Comment')
const mongoose = require('mongoose')

const _ = require('lodash')
let server
let mongod
let db, validID

describe('Userss', () => {
    // var token
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: './test/database',
                    dbName: 'bbsdb' // by default generate random dbName
                }
            },{ debug: true })
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            mongoose.connect('mongodb://localhost:27017/bbsdb', {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            server = require('../../../bin/www')
            db = mongoose.connection
        } catch (error) {
            console.log(error)
        }
    })

    after(async () => {
        try {
            await db.dropDatabase()
        } catch (error) {
            console.log(error)
        }
    })

    beforeEach(async () => {
        try {
            await User.deleteMany({})
            let user = new User()
            user.username = 'test1'
            user.email = 'test1@test.com'
            user.password = 'test123456'
            user.createdTime = new Date
            await user.save()
            user = new User()
            user.username = 'test2'
            user.email = 'test2@test.com'
            user.password = 'test123456'
            user.createdTime = new Date
            await user.save()
            user = await User.findOne({username: 'test1'})
            validID =  user._id
        } catch (error) {
            console.log(error)
        }
    })

    describe('POST /user/login', () => {
        const user = new User({
            username: 'test1',
            password: 'test123456'
        })
        it('should Login in successfully', done => {
            request(server)
                .post('/user/login')
                .send(user)
                .expect(200)
                .end((err, res) => {
                    try {
                        expect(res.body).to.be.a('object')
                        expect(res.body.code).to.equal(1)
                        expect(res.body.token).to.be.not.null
                        // token = 'Bearer ' + res.body.token
                        done()
                    }catch (e) {
                        done(e)
                    }
                })
        })
    })
    //TODO:test register
    describe('GET /users', () => {
        it('should GET all the users', done => {
            request(server)
                .get('/users')
                .set('Accept', 'application/json')
                // .set('Authorization',token)
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err,res) => {
                    try {
                        expect(res.body).to.be.a('array')
                        expect(res.body.length).to.equal(2)
                        let result = _.map(res.body, user => {
                            return{
                                username: user.username,
                                email: user.email
                            }
                        })
                        expect(result).to.deep.include({
                            username: 'test1',
                            email: 'test1@test.com'
                        })
                        expect(result).to.deep.include({
                            username: 'test2',
                            email: 'test2@test.com'
                        })
                        done()
                    }catch (e) {
                        done(e)
                    }
                })
        })
    })

    describe('GET /user/:id', () => {
        it('should return the user information', function () {
            request(server)
                .get(`/user/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.user).to.deep.include({username: 'test1',email: 'test1@test.com'})
                })
        })
    })


    describe('DELETE /user/delete/:id', () => {
        it('should delete the user', function () {
            request(server)
                .delete(`/user/delete/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body).to.deep.not.include({username: 'test1',email: 'test1@test.com'})
                })
        })
    })

})


describe('Posts', () => {
    // var token
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: './test/database',
                    dbName: 'bbsdb' // by default generate random dbName
                }
            },{ debug: true })
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            mongoose.connect('mongodb://localhost:27017/bbsdb', {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            server = require('../../../bin/www')
            db = mongoose.connection
        } catch (error) {
            console.log(error)
        }
    })

    after(async () => {
        try {
            await db.dropDatabase()
        } catch (error) {
            console.log(error)
        }
    })

    beforeEach(async () => {
        try {
            await Post.deleteMany({})
            let post = new Post()
            post.title = 'title1'
            post.content = 'This is content1'
            post.creatorId = '5db4b196869302154c6f0e71'
            post.createdTime = new Date
            post.updatedTime = new Date
            await post.save()
            post = new Post()
            post.title = 'title2'
            post.content = 'This is content2'
            post.creatorId = '5db4b196869302154c6f0e71'
            post.createdTime = new Date
            post.updatedTime = new Date
            await post.save()
            post = await Post.findOne({title: 'title1'})
            validID = post._id
        } catch (error) {
            console.log(error)
        }
    })

    describe('POST /post', () => {
        it('should post successfully', done => {
            const post = new Post({
                title: 'title3',
                content: 'This is content3',
                creatorId: '5db4b196869302154c6f0e71',
                createdTime: new Date,
                updatedTime: new Date
            })
            request(server)
                .post('/post')
                .send(post)
                .expect(200)
                .end((err, res) => {
                    try {
                        expect(res.body).to.be.a('object')
                        expect(res.body.code).to.equal(1)
                        done()
                    }catch (e) {
                        done(e)
                    }
                })
        })
    })

    describe('GET /post/:id', () => {
        it('should return the post detail', function () {
            request(server)
                .get(`/post/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.post).to.deep.include({title: 'title1',content: 'This is content1'})
                })
        })
    })

    describe('GET /all', () => {
        it('should return all the posts', function () {
            request(server)
                .get('/all')
                // .set('Accept', 'application/json')
                .expect(200)
                .end((err, res) => {
                    expect(res.body.posts).to.be.a('array')
                    expect(res.body.posts.length).to.equal(2)
                })
        })
    })

    describe('GET /search', () => {
        it('should return all the related posts', function () {
            request(server)
                .get('/search/1')
                .expect(200)
                .end((err, res) => {
                    expect(res.body.posts).to.be.a('array')
                    expect(res.body.posts.length).to.equal(1)
                    expect(res.body.posts[0]).to.deep.include({title: 'title1',content: 'This is content1'})
                })
        })
    })

    describe('DELETE /post/delete/:id', () => {
        it('should delete the post', function () {
            request(server)
                .delete(`/post/delete/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body).to.deep.not.include({title: 'title1',content: 'This is content1'})
                })
        })
    })

    describe('PUT /post/update', () => {
        it('should update the post', function () {
            const body = {
                id: validID,
                title: 'updatedTitle1',
                content: 'This is updatedContent1'
            }
            request(server)
                .put('/post/update')
                .send(body)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body.post).to.deep.include({title: 'updatedTitle1',content: 'This is updatedContent1'})
                })
        })
    })
})

describe('Comments', () => {
    // var token
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: './test/database',
                    dbName: 'bbsdb' // by default generate random dbName
                }
            },{ debug: true })
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            mongoose.connect('mongodb://localhost:27017/bbsdb', {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            server = require('../../../bin/www')
            db = mongoose.connection
        } catch (error) {
            console.log(error)
        }
    })

    after(async () => {
        try {
            await db.dropDatabase()
        } catch (error) {
            console.log(error)
        }
    })

    beforeEach(async () => {
        try {
            await Comment.deleteMany({})
            let comment = new Comment()
            comment.parentId = '5db4b196869302154c6f0e71'
            comment.content = 'This is comment1'
            comment.creatorId = '5db4b196869302154c6f0e71'
            comment.createdTime = new Date
            await comment.save()
            comment = new Comment()
            comment.parentId = '5db4b196869302154c6f0e71'
            comment.content = 'This is comment2'
            comment.creatorId = '5db4b196869302154c6f0e71'
            comment.createdTime = new Date
            await comment.save()
            comment = await Comment.findOne({content: 'This is comment1'})
            validID = comment._id
        } catch (error) {
            console.log(error)
        }
    })

    describe('Comment /comment', () => {
        it('should comment successfully', done => {
            const comment = new Comment({
                parentId: '5db4b196869302154c6f0e71',
                content: 'This is comment3',
                creatorId: '5db4b196869302154c6f0e71',
                createdTime: new Date
            })
            request(server)
                .post('/comment')
                .send(comment)
                .expect(200)
                .end((err, res) => {
                    try {
                        expect(res.body).to.be.a('object')
                        expect(res.body.code).to.equal(1)
                        done()
                    }catch (e) {
                        done(e)
                    }
                })
        })
    })

    describe('DELETE /comment/delete/:id', () => {
        it('should delete the post', function () {
            request(server)
                .delete(`/comment/delete/${validID}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.code).to.equal(1)
                    expect(res.body).to.deep.not.include({content: 'This is comment1'})
                })
        })
    })
})
