# WIT_BBS

WITBBS is a online commuity for WIT students communicating.  

 Students can post some information on this app, including secondhand goods, house for renting and some learning skills.
 And other students can comment these posts.

## About Author
Name: Chengming Fan  
ID: 20086437

## Preview
  This project is deployed in the heroku  
  click to preview: [witbbs](https://witbbs.herokuapp.com/)

## Functions
   1. register
   2. login
   3. update information(eg.password)
   4. post a post
   5. update a post
   6. delete a post
   7. comment a post
   8. delete comment
   9. search posts by keyword
   10. get all users and posts(for administrator)
   11. delete a user(for administrator)

## Features
* use mongodb atlas to store the users, posts and comments.
* use bcrypt to encrypt the password
* use jwt to verify whether the user is login
* Automated Testing

## Git link
  BitBucket: [bbs_wit](https://bitbucket.org/fantasy1999/wit_bbs/src/master/)

## References
  [bcrypt](https://github.com/kelektiv/node.bcrypt.js/)  
  [jsonwebtoken](https://jwt.io/)  
  [mocha](https://mochajs.org/)

  
