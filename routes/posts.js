let Post = require('../models/Post')
let Comment = require('../models/Comment')

module.exports = {
    post(req, res) {
        const time = new Date()
        const post = new Post({
            title: req.body.title,
            content: req.body.content,
            creatorId: req.body.creatorId,
            createdTime: time,
            updatedTime: time
        })
        post.save((err, docs) => {
            if (err) {
                console.log(err)
                res.send({code: 0, msg: 'Failed to post'})
            } else {
                res.send({code: 1, msg: 'Successful to post!'})
            }
        })
    },
    getPostDetail(req, res) {
        const post_id = req.params.id
        Post.findById(post_id, function (err, post) {
            if (err) {
                res.send({code:0, msg: 'Failed to get data: ' + err})
            } else if (post) {
                Comment.find({'parentId': post._id}, function (err, comments) {
                    res.send({
                        post: post,
                        comments: comments
                    })
                    //update viewCount of the post
                    Post.updateOne({'_id': post._id}, {$inc: {'viewCount': 1}}, (err, docs) => {
                        if (err)
                            res.send({code:0, msg:err})
                    })
                })
            } else {
                res.send({msg: 'No This Post'})
            }
        })
    },
    getAllPosts(req, res) {
        Post.find(function (err, posts) {
            if (err)
                res.send(err)
            res.send({code:1, posts: posts})
        })
    },
    // fuzzy search
    searchPosts (req, res) {
        const keyword = req.params.keyword
        const reg = new RegExp(keyword, 'i')
        Post.find(
            {title: {$regex: reg}},
            function (err, posts) {
                if(err) {
                    res.send({code: 0, msg:err})
                }
                res.send({code: 1, posts: posts})
            })
    },
    deletePost (req, res) {
        const id = req.params.id
        Post.findByIdAndDelete(id, (error, docs) => {
            if (error)
                res.send({code: 0, msg: 'Failed to delete'})
            if (docs)
                res.send({code: 1, msg: 'Successful to delete'})
            else
                res.send({code: 0, msg: 'No Related Data'})
        })
    },
    updatePost (req, res) {
        const post = new Post({
            _id: req.body.id,
            title: req.body.title,
            content: req.body.content,
            updatedTime: new Date
        })
        const id = post.id
        Post.findOneAndUpdate({_id:id}, post,{new: true},function (err, post) {
            if(err)
                res.send({code:0, msg: 'Failed to update: ' + err})
            if(post)
                res.send({code: 1, msg: 'Successful to update!',post: post})
            else
                res.send({code: 0, msg: 'No this post'})
        })
    }
}
