const mongoose = require('mongoose')
const DB_URL = 'mongodb+srv://fan:f12345678@witbbs-lrlpo.mongodb.net/WIT_BBS_db?retryWrites=true&w=majority'

mongoose.set('useCreateIndex',true)
mongoose.set('useFindAndModify',false)
mongoose.connect(DB_URL,{ useNewUrlParser: true })

let db = mongoose.connection

db.on('open', function () {
    console.log('Successfully to Connect to [' + db.name +']')
})

db.on('error', function (err) {
    console.log('Unable to Connect to [' + db.name + ']', err)
})

module.exports = mongoose



